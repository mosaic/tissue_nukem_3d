Install
=======

Download sources and use setup::

    $ python setup.py install
    or
    $ python setup.py develop


Use
===

Simple usage:

.. code-block:: python

    import tissue_nukem_3d


Contribute
==========

Fork this project on github_

.. _github: https://gitlab.inria.fr/mosaic/tissue_nukem_3d



Acknowledgments
===============
