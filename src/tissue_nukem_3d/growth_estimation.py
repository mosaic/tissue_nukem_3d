import numpy as np
from scipy import ndimage as nd
import pandas as pd
from sklearn import linear_model

from cellcomplex.property_topomesh.utils.delaunay_tools import delaunay_triangulation
from cellcomplex.property_topomesh.creation import vertex_topomesh, triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces, compute_topomesh_vertex_property_from_cells

from cellcomplex.property_topomesh.extraction import epidermis_topomesh, topomesh_connected_components, cut_surface_topomesh, clean_topomesh
from cellcomplex.property_topomesh.utils.geometry_tools import tetra_geometric_features
from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe

from tissue_nukem_3d.epidermal_maps import nuclei_density_function, compute_local_2d_signal

from cellcomplex.utils import array_dict

from timagetk.components import SpatialImage
from timagetk.algorithms.blockmatching import blockmatching

from copy import deepcopy
from time import time as current_time

import logging

from tissue_nukem_3d.nuclei_mesh_tools import nuclei_tetrahedrization_topomesh


def image_sequence_rigid_vectorfield_registration(reference_images, microscope_orientation=-1, verbose=True, debug=False, loglevel=0):
    """
    """
    
    filenames = np.sort(list(reference_images.keys()))

    transformed_images = {}
    transformed_images[filenames[0]] = reference_images[filenames[0]]

    rigid_transformations = {}

    vectorfield_transformations = {}

    for i_file,(reference_filename,floating_filename) in enumerate(zip(filenames[:-1],filenames[1:])):

        logging.info("".join(["  " for l in range(loglevel)])+"--> Computing transformations "+reference_filename+" <--> "+floating_filename)
        
        reference_img = reference_images[reference_filename] if i_file==0 else transformed_images[reference_filename]
        reference_img = SpatialImage(reference_img,voxelsize=reference_img.voxelsize)

        floating_img = reference_images[floating_filename]
        floating_img = SpatialImage(floating_img,voxelsize=floating_img.voxelsize)
        
        size = np.array(reference_img.shape)
        resolution = microscope_orientation*np.array(reference_img.voxelsize)
        
        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing rigid transformation "+reference_filename[-3:]+" <-- "+floating_filename[-3:])
        transform, transform_img = blockmatching(floating_img, reference_img, param_str_1 ='-trsf-type rigid')
        #transform, transform_img = registration(floating_img, reference_img, transformation_type='rigid')
        
        transformed_images[floating_filename] = transform_img

        rigid_matrix = transform.mat.to_np_array().astype(float)
        rigid_transformations[(reference_filename,floating_filename)] = rigid_matrix

        invert_rigid_matrix = np.linalg.inv(rigid_matrix)
        rigid_transformations[(floating_filename,reference_filename)] = invert_rigid_matrix

        logging.info("".join(["  " for l in range(loglevel)])+"  <-- Computing rigid transformation "+reference_filename[-3:]+" <-- "+floating_filename[-3:]+" ["+str(current_time()-start_time)+" s]")


        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing vectorfield transformation "+reference_filename[-3:]+" <-- "+floating_filename[-3:])
        blockmatching_transform, blockmatching_img = blockmatching(transform_img, reference_img,  param_str_1 ='-trsf-type vectorfield')
        #blockmatching_transform, blockmatching_img = registration(transform_img, reference_img,  transformation_type='vectorfield')
        #vector_field = np.transpose([blockmatching_transform.vx.to_spatial_image(),blockmatching_transform.vy.to_spatial_image(),blockmatching_transform.vz.to_spatial_image()],(1,2,3,0))
        logging.info("".join(["  " for l in range(loglevel)])+"  <-- Computing vectorfield transformation "+reference_filename[-3:]+" <-- "+floating_filename[-3:]+" ["+str(current_time()-start_time)+" s]")
        
        # vectorfield_transformations[(floating_filename,reference_filename)] = SpatialImage(vector_field,voxelsize=transformed_images[floating_filename].voxelsize)
        # vectorfield_transformations[(reference_filename,floating_filename)] = SpatialImage(vector_field,voxelsize=transformed_images[floating_filename].voxelsize)
        vectorfield_transformations[(reference_filename,floating_filename)] = blockmatching_transform

        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing vectorfield transformation "+reference_filename[-3:]+" --> "+floating_filename[-3:])
        invert_blockmatching_transform, invert_blockmatching_img = blockmatching(reference_img, transform_img, param_str_1 ='-trsf-type vectorfield')
        #invert_blockmatching_transform, invert_blockmatching_img = registration(reference_img, transform_img,  transformation_type='vectorfield')
        invert_vector_field = np.transpose([invert_blockmatching_transform.vx.to_spatial_image(),invert_blockmatching_transform.vy.to_spatial_image(),invert_blockmatching_transform.vz.to_spatial_image()],(1,2,3,0))
        logging.info("".join(["  " for l in range(loglevel)])+"  <-- Computing vectorfield transformation "+reference_filename[-3:]+" --> "+floating_filename[-3:]+" ["+str(current_time()-start_time)+" s]")
        
        # vectorfield_transformations[(reference_filename,floating_filename)] = SpatialImage(invert_vector_field,voxelsize=transformed_images[floating_filename].voxelsize)
        # vectorfield_transformations[(floating_filename,reference_filename)] = SpatialImage(invert_vector_field,voxelsize=transformed_images[floating_filename].voxelsize)
        vectorfield_transformations[(floating_filename, reference_filename)] = invert_blockmatching_transform

    return transformed_images, rigid_transformations, vectorfield_transformations


def spherical_2d_projection(positions, center_offset=3):
    """
    """

    center = np.array(list(positions.values())).mean(axis=0)
    center[2] -= center_offset*(positions.values()-positions.values().mean(axis=0))[:,2].max()
    
    point_vectors = positions.values() - center
    point_r = np.linalg.norm(point_vectors,axis=1)
    point_rx = np.linalg.norm(point_vectors[:,np.array([0,2])],axis=1)
    point_ry = np.linalg.norm(point_vectors[:,np.array([1,2])],axis=1)
    
    point_phi = np.sign(point_vectors[:,0])*np.arccos(point_vectors[:,2]/point_rx)
    point_psi = np.sign(point_vectors[:,1])*np.arccos(point_vectors[:,2]/point_ry)
    
    spherical_positions = deepcopy(positions)
    for i,c in enumerate(positions.keys()):
        spherical_positions[c][0] = point_phi[i]
        spherical_positions[c][1] = point_psi[i]
        spherical_positions[c][2] = 0.

    return spherical_positions


def apply_sequence_point_registration(sequence_data, sequence_rigid_transforms, microscope_orientation=-1, verbose=False, debug=False, loglevel=0):
    """

    :param sequence_data:
    :param sequence_rigid_transforms:
    :param microscope_orientation:
    :param verbose:
    :param debug:
    :param loglevel:
    :return:
    """

    filenames = np.sort(list(sequence_data.keys()))

    for i_file, (reference_filename, floating_filename) in enumerate(zip(filenames[:-1], filenames[1:])):
        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)]) + "--> Computing registered points " + reference_filename + " --> " + floating_filename)

        reference_data = sequence_data[reference_filename]
        floating_data = sequence_data[floating_filename]

        X = reference_data['center_x']
        Y = reference_data['center_y']
        Z = reference_data['center_z']

        if i_file == 0:
            reference_data['registered_x'] = X
            reference_data['registered_y'] = Y
            reference_data['registered_z'] = Z

        invert_rigid_matrix = sequence_rigid_transforms[(floating_filename, reference_filename)]

        X = floating_data['center_x']
        Y = floating_data['center_y']
        Z = floating_data['center_z']

        cell_barycenters = array_dict(np.transpose([X, Y, Z]), floating_data.index.values)

        homogeneous_points = np.concatenate([microscope_orientation * np.transpose([X, Y, Z]), np.ones((len(X), 1))], axis=1)
        registered_points = np.einsum("...ij,...j->...i", invert_rigid_matrix, homogeneous_points)
        registered_points = microscope_orientation * registered_points[:, :3]

        floating_data['registered_x'] = registered_points[:, 0]
        floating_data['registered_y'] = registered_points[:, 1]
        floating_data['registered_z'] = registered_points[:, 2]

        logging.info("".join(["  " for l in range(loglevel)])+"<-- Computing registered points "+reference_filename+" <-- "+floating_filename+" ["+str(current_time()-start_time)+" s]")

    return sequence_data


def surfacic_growth_estimation(sequence_data, sequence_rigid_transforms, sequence_vectorfield_transforms, maximal_length=15., microscope_orientation=-1, quantified_signals=[], verbose=False, debug=False, loglevel=0):
    """
    """

    filenames = np.sort(list(sequence_data.keys()))
    print(filenames)

    previous_transform = np.diag(np.ones(4))
        
    for i_file,(reference_filename,floating_filename) in enumerate(zip(filenames[:-1],filenames[1:])):
        
        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"--> Computing next surfacic growth "+reference_filename+" --> "+floating_filename)      

        rigid_matrix = sequence_rigid_transforms[(reference_filename,floating_filename)]
        invert_rigid_matrix = sequence_rigid_transforms[(floating_filename,reference_filename)]

        vector_field_transform = sequence_vectorfield_transforms[(floating_filename,reference_filename)]
        vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(), vector_field_transform.vy.to_spatial_image(), vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        vector_field_voxelsize = vector_field_transform.vx.to_spatial_image().voxelsize

        invert_vector_field_transform = sequence_vectorfield_transforms[(reference_filename,floating_filename)]
        invert_vector_field = np.transpose([invert_vector_field_transform.vx.to_spatial_image(), invert_vector_field_transform.vy.to_spatial_image(), invert_vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        invert_vector_field_voxelsize = invert_vector_field_transform.vx.to_spatial_image().voxelsize

        size = np.array(vector_field.shape)[:3]
        resolution = microscope_orientation*np.array(vector_field_voxelsize)
            
        reference_data = sequence_data[reference_filename]    
        floating_data = sequence_data[floating_filename]    

        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing triangulation on "+reference_filename[-3:])      

        X = reference_data['center_x']
        Y = reference_data['center_y']
        Z = reference_data['center_z']
        
        if i_file == 0:
            reference_data['registered_x'] = X
            reference_data['registered_y'] = Y
            reference_data['registered_z'] = Z
    
        reference_points = np.transpose([X,Y,Z])

        cell_barycenters = array_dict(np.transpose([X,Y,Z]),reference_data.index.values)
        cell_flat_barycenters = spherical_2d_projection(cell_barycenters)
            
        triangles = np.array(cell_barycenters.keys())[delaunay_triangulation(np.array([cell_flat_barycenters[c] for c in cell_barycenters.keys()]))]
        reference_triangulation_topomesh = triangle_topomesh(triangles, cell_barycenters)
        
        compute_topomesh_property(reference_triangulation_topomesh,'length',1)
        compute_topomesh_property(reference_triangulation_topomesh,'faces',1)
        
        boundary_edges = np.array(list(map(len,reference_triangulation_topomesh.wisp_property('faces',1).values())))==1
        distant_edges = reference_triangulation_topomesh.wisp_property('length',1).values() > maximal_length
        edges_to_remove = np.array(list(reference_triangulation_topomesh.wisps(1)))[boundary_edges & distant_edges]
        
        while len(edges_to_remove) > 0:
            triangles_to_remove = np.unique(np.concatenate(reference_triangulation_topomesh.wisp_property('faces',1).values(edges_to_remove)))
            for t in triangles_to_remove:
                reference_triangulation_topomesh.remove_wisp(2,t)
            
            clean_topomesh(reference_triangulation_topomesh)
            
            compute_topomesh_property(reference_triangulation_topomesh,'faces',1)
        
            boundary_edges = np.array(list(map(len,reference_triangulation_topomesh.wisp_property('faces',1).values())))==1
            distant_edges = reference_triangulation_topomesh.wisp_property('length',1).values() > maximal_length
            edges_to_remove = np.array(list(reference_triangulation_topomesh.wisps(1)))[boundary_edges & distant_edges]
        
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Transforming triangulation to "+floating_filename[-3:])     

        invert_registered_triangulation_topomesh = deepcopy(reference_triangulation_topomesh)
        
        image_coords = tuple(np.transpose(np.minimum(size-1,np.maximum(0,(reference_points/resolution).astype(int)))))
        point_displacement = invert_vector_field[image_coords]
        
        for i_dim,dim in enumerate(['x','y','z']):
            if "next_motion_"+dim in reference_data.columns:
                del reference_data['next_motion_'+dim]
            reference_data['next_motion_'+dim] = point_displacement[:,i_dim]
            if i_file == 0:
                if "previous_motion_"+dim in reference_data.columns:
                    del reference_data['previous_motion_'+dim]
                reference_data['previous_motion_'+dim]=0
        
        invert_registered_points = reference_points + point_displacement
        invert_registered_barycenters = array_dict(invert_registered_points,reference_data.index.values)

        invert_registered_triangulation_topomesh.update_wisp_property('barycenter',0,invert_registered_barycenters)
        
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing triangle area ratio "+floating_filename[-3:]+"/"+reference_filename[-3:])

        compute_topomesh_property(reference_triangulation_topomesh,'area',2)

        compute_topomesh_property(invert_registered_triangulation_topomesh,'length',1)
        compute_topomesh_property(invert_registered_triangulation_topomesh,'area',2)

        area_growth = invert_registered_triangulation_topomesh.wisp_property('area',2).values() - reference_triangulation_topomesh.wisp_property('area',2).values() 
        relative_area_growth = invert_registered_triangulation_topomesh.wisp_property('area',2).values()/reference_triangulation_topomesh.wisp_property('area',2).values() 

        reference_triangulation_topomesh.update_wisp_property('relative_surfacic_growth',2,array_dict(relative_area_growth,reference_triangulation_topomesh.wisp_property('area',2).keys()))
        compute_topomesh_vertex_property_from_faces(reference_triangulation_topomesh,'relative_surfacic_growth',neighborhood=3,adjacency_sigma=1.2)

        relative_future_surfacic_growth = reference_triangulation_topomesh.wisp_property('relative_surfacic_growth',0).values(reference_data.index.values)
        
        reference_data['next_relative_surfacic_growth'] = relative_future_surfacic_growth
        if i_file == 0:
            reference_data['previous_relative_surfacic_growth'] = np.nan

        logging.info("".join(["  " for l in range(loglevel)])+"<-- Computing next surfacic growth "+reference_filename+" --> "+floating_filename+" ["+str(current_time()-start_time)+" s]")
        
        
        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"--> Computing previous surfacic growth "+reference_filename+" <-- "+floating_filename)

        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing triangulation on "+floating_filename[-3:])    

        X = floating_data['center_x']
        Y = floating_data['center_y']
        Z = floating_data['center_z']
        
        cell_barycenters = array_dict(np.transpose([X,Y,Z]),floating_data.index.values)

        homogeneous_points = np.concatenate([microscope_orientation*np.transpose([X,Y,Z]), np.ones((len(X),1))],axis=1)
        registered_points = np.einsum("...ij,...j->...i",invert_rigid_matrix, homogeneous_points)
        registered_points = microscope_orientation*registered_points[:,:3]
        
        previous_transform = np.dot(invert_rigid_matrix,previous_transform)
        sequence_registered_points = np.einsum("...ij,...j->...i",previous_transform,homogeneous_points)
        sequence_registered_points = microscope_orientation*sequence_registered_points[:,:3]

        floating_data['registered_x']=registered_points[:,0]
        floating_data['registered_y']=registered_points[:,1]
        floating_data['registered_z']=registered_points[:,2]
        
        registered_barycenters = array_dict(registered_points,floating_data.index.values)
            
        cell_flat_barycenters = spherical_2d_projection(cell_barycenters)
        
        triangles = np.array(cell_barycenters.keys())[delaunay_triangulation(np.array([cell_flat_barycenters[c] for c in cell_barycenters.keys()]))]
        transform_triangulation_topomesh = triangle_topomesh(triangles, registered_barycenters)

        compute_topomesh_property(transform_triangulation_topomesh,'length',1)
        compute_topomesh_property(transform_triangulation_topomesh,'faces',1)
        
        boundary_edges = np.array(list(map(len,transform_triangulation_topomesh.wisp_property('faces',1).values())))==1
        distant_edges = transform_triangulation_topomesh.wisp_property('length',1).values() > maximal_length
        edges_to_remove = np.array(list(transform_triangulation_topomesh.wisps(1)))[boundary_edges & distant_edges]
        
        while len(edges_to_remove) > 0:
            triangles_to_remove = np.unique(np.concatenate(transform_triangulation_topomesh.wisp_property('faces',1).values(edges_to_remove)))
            for t in triangles_to_remove:
                transform_triangulation_topomesh.remove_wisp(2,t)
            
            clean_topomesh(transform_triangulation_topomesh)
            
            compute_topomesh_property(transform_triangulation_topomesh,'faces',1)
        
            boundary_edges = np.array(list(map(len,transform_triangulation_topomesh.wisp_property('faces',1).values())))==1
            distant_edges = transform_triangulation_topomesh.wisp_property('length',1).values() > maximal_length
            edges_to_remove = np.array(list(transform_triangulation_topomesh.wisps(1)))[boundary_edges & distant_edges]
        
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Transforming triangulation to "+reference_filename[-3:])   
        
        registered_triangulation_topomesh = deepcopy(transform_triangulation_topomesh)

        image_coords = tuple(np.transpose(np.minimum(size-1,np.maximum(0,(registered_points/resolution).astype(int)))))
        point_displacement = vector_field[image_coords]
        
        for i_dim,dim in enumerate(['x','y','z']):
            if "previous_motion_"+dim in floating_data.columns:
                del floating_data['previous_motion_'+dim]
            floating_data['previous_motion_'+dim] = point_displacement[:,i_dim]
            if i_file == len(filenames)-2:
                if 'next_motion_'+dim in floating_data.columns:
                    del floating_data['next_motion_'+dim]
                floating_data['next_motion_'+dim]=0
                
        floating_data['previous_motion_x'] = point_displacement[:,0]
        floating_data['previous_motion_y'] = point_displacement[:,1]
        floating_data['previous_motion_z'] = point_displacement[:,2]
        
        if i_file == len(filenames)-2:
            floating_data['next_motion_x'] = 0
            floating_data['next_motion_y'] = 0
            floating_data['next_motion_z'] = 0
        
        registered_points = registered_points + point_displacement
        registered_barycenters = array_dict(registered_points,floating_data.index.values)
        
        registered_triangulation_topomesh.update_wisp_property('barycenter',0,registered_barycenters)
        
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Computing triangle area ratio "+floating_filename[-3:]+"/"+reference_filename[-3:])

        compute_topomesh_property(transform_triangulation_topomesh,'area',2)

        compute_topomesh_property(registered_triangulation_topomesh,'length',1)
        compute_topomesh_property(registered_triangulation_topomesh,'area',2)
        
        area_growth = transform_triangulation_topomesh.wisp_property('area',2).values() - registered_triangulation_topomesh.wisp_property('area',2).values() 
        relative_area_growth = transform_triangulation_topomesh.wisp_property('area',2).values()/registered_triangulation_topomesh.wisp_property('area',2).values() 

        registered_triangulation_topomesh.update_wisp_property('relative_surfacic_growth',2,array_dict(relative_area_growth,transform_triangulation_topomesh.wisp_property('area',2).keys()))
        compute_topomesh_vertex_property_from_faces(registered_triangulation_topomesh,'relative_surfacic_growth',neighborhood=3,adjacency_sigma=1.2)
        relative_surfacic_growth = registered_triangulation_topomesh.wisp_property('relative_surfacic_growth',0).values(floating_data.index.values)

        # transform_triangulation_topomesh.update_wisp_property('relative_surfacic_growth',2,array_dict(relative_area_growth,transform_triangulation_topomesh.wisp_property('area',2).keys()))
        # compute_topomesh_vertex_property_from_faces(transform_triangulation_topomesh,'relative_surfacic_growth',neighborhood=3,adjacency_sigma=1.2)
        # relative_surfacic_growth = transform_triangulation_topomesh.wisp_property('relative_surfacic_growth',0).values(floating_data.index.values)
        
        floating_data['previous_relative_surfacic_growth'] = relative_surfacic_growth
        if i_file == len(filenames)-2:
            floating_data['next_relative_surfacic_growth'] = np.nan
        
        logging.info("".join(["  " for l in range(loglevel)])+"<-- Computing previous surfacic growth "+reference_filename+" <-- "+floating_filename+" ["+str(current_time()-start_time)+" s]")


        # variable_names = ['DIIV','Normalized_DIIV','Auxin','Normalized Auxin','DR5','Normalized_DR5','CLV3','TagBFP','Normalized_TagBFP','mean_curvature','gaussian_curvature']
        variable_names = [c for c in reference_data.columns if c in quantified_signals]

        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"--> Estimating next signal values "+reference_filename[-3:]+" --> "+floating_filename[-3:]+" "+str(variable_names))

        floating_X = floating_data['registered_x'].values
        floating_Y = floating_data['registered_y'].values
        
        reference_next_X = invert_registered_points[:,0]
        reference_next_Y = invert_registered_points[:,1]

        for var in variable_names:
            if (var in reference_data.columns) and (var in floating_data.columns) and not('_growth' in var):
                floating_var = floating_data[var].values
                reference_next_var = compute_local_2d_signal(np.transpose([floating_X,floating_Y]),np.transpose([reference_next_X,reference_next_Y]),floating_var)
                reference_data['next_'+var] = reference_next_var

                if i_file == len(filenames)-2:
                    floating_data['next_'+var] = np.nan
        

        logging.info("".join(["  " for l in range(loglevel)])+"<--> Estimating next signal values "+reference_filename[-3:]+" --> "+floating_filename[-3:]+" ["+str(current_time()-start_time)+" s]")

        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"--> Estimating previous signal values "+reference_filename[-3:]+" <-- "+floating_filename[-3:]+" "+str(variable_names))

        reference_X = reference_data['center_x'].values
        reference_Y = reference_data['center_y'].values
        
        floating_previous_X = registered_points[:,0]
        floating_previous_Y = registered_points[:,1]

        for var in variable_names:
            if (var in reference_data.columns) and (var in floating_data.columns) and not('_growth' in var):
                reference_var = reference_data[var].values
                floating_previous_var = compute_local_2d_signal(np.transpose([reference_X,reference_Y]),np.transpose([floating_previous_X,floating_previous_Y]),reference_var)
                floating_data['previous_'+var] = floating_previous_var

                if i_file == 0:
                    reference_data['previous_'+var] = np.nan

        logging.info("".join(["  " for l in range(loglevel)])+"--> Estimating previous signal values "+reference_filename[-3:]+" <-- "+floating_filename[-3:]+" ["+str(current_time()-start_time)+" s]")

    return sequence_data


def volumetric_growth_estimation(sequence_data, sequence_rigid_transforms, sequence_vectorfield_transforms, maximal_length=15., microscope_orientation=-1, quantified_signals=[], verbose=False, debug=False, loglevel=0):
    """
    """

    filenames = np.sort(sequence_data.keys())
    print(filenames)

    previous_transform = np.diag(np.ones(4))

    sequence_triangulations = {}

    for i_file, (reference_filename, floating_filename) in enumerate(zip(filenames[:-1], filenames[1:])):

        start_time = current_time()
        logging.info("".join(["  " for l in range(loglevel)])+"--> Computing next volumetric growth "+reference_filename+" --> "+floating_filename)

        rigid_matrix = sequence_rigid_transforms[(reference_filename,floating_filename)]
        invert_rigid_matrix = sequence_rigid_transforms[(floating_filename,reference_filename)]

        vector_field_transform = sequence_vectorfield_transforms[(floating_filename,reference_filename)]
        vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(), vector_field_transform.vy.to_spatial_image(), vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        vector_field_voxelsize = vector_field_transform.vx.to_spatial_image().voxelsize

        invert_vector_field_transform = sequence_vectorfield_transforms[(reference_filename,floating_filename)]
        invert_vector_field = np.transpose([invert_vector_field_transform.vx.to_spatial_image(), invert_vector_field_transform.vy.to_spatial_image(), invert_vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        invert_vector_field_voxelsize = invert_vector_field_transform.vx.to_spatial_image().voxelsize

        size = np.array(vector_field.shape)[:3]
        resolution = microscope_orientation * np.array(vector_field_voxelsize)
        voxelsize = np.array(vector_field_voxelsize)

        reference_data = sequence_data[reference_filename]
        floating_data = sequence_data[floating_filename]

        X = reference_data['center_x']
        Y = reference_data['center_y']
        Z = reference_data['center_z']

        if i_file == 0:
            reference_data['transformed_x'] = X
            reference_data['transformed_y'] = Y
            reference_data['transformed_z'] = Z

        reference_points = np.transpose([X, Y, Z])
        reference_positions = array_dict(reference_points, reference_data.index.values)

        if not reference_filename in sequence_triangulations.keys():
            reference_triangulation_topomesh = nuclei_tetrahedrization_topomesh(reference_data, maximal_distance=maximal_length)
        else:
            reference_triangulation_topomesh = sequence_triangulations[reference_filename]

        compute_topomesh_property(reference_triangulation_topomesh, 'vertices', 3)
        reference_tetras = reference_triangulation_topomesh.wisp_property('vertices', 3).values(list(reference_triangulation_topomesh.wisps(3)))
        reference_tetra_points = reference_positions.values(reference_tetras)
        reference_tetra_volumes = tetra_geometric_features(reference_tetras, reference_positions, ['volume'])[:, 0]
        reference_triangulation_topomesh.update_wisp_property('volume', 3, array_dict(reference_tetra_volumes, list(reference_triangulation_topomesh.wisps(3))))

        invert_registered_triangulation_topomesh = deepcopy(reference_triangulation_topomesh)

        image_coords = tuple(np.transpose(np.minimum(size - 1, np.maximum(0, (reference_points / resolution).astype(int)))))
        point_displacement = invert_vector_field[image_coords]

        for i_dim, dim in enumerate(['x', 'y', 'z']):
            if "next_motion_" + dim in reference_data.columns:
                del reference_data['next_motion_' + dim]
            reference_data['next_motion_' + dim] = point_displacement[:, i_dim]
            if i_file == 0:
                if "previous_motion_" + dim in reference_data.columns:
                    del reference_data['previous_motion_' + dim]
                reference_data['previous_motion_' + dim] = 0

        invert_registered_points = reference_points + point_displacement
        invert_registered_positions = array_dict(invert_registered_points, reference_data.index.values)
        invert_registered_tetra_points = invert_registered_positions.values(reference_tetras)

        invert_registered_triangulation_topomesh.update_wisp_property('barycenter', 0, invert_registered_positions)

        invert_registered_tetra_volumes = tetra_geometric_features(reference_tetras, invert_registered_positions, ['volume'])[:, 0]
        invert_registered_triangulation_topomesh.update_wisp_property('volume', 3, array_dict(invert_registered_tetra_volumes, list(reference_triangulation_topomesh.wisps(3))))

        reference_volumetric_growth_values = invert_registered_tetra_volumes / reference_tetra_volumes

        reference_centered_tetra_points = reference_tetra_points - np.mean(reference_tetra_points, axis=1)[:, np.newaxis]
        invert_registered_centered_tetra_points = invert_registered_tetra_points - np.mean(invert_registered_tetra_points, axis=1)[:, np.newaxis]

        reference_tetra_strain_matrix = []
        reference_tetra_stretch_matrix = []
        reference_tetra_strain_values = []
        reference_tetra_stretch_axes = []
        for reference_tet, invert_registered_tet in zip(reference_centered_tetra_points, invert_registered_centered_tetra_points):
            regr = linear_model.Ridge(alpha=.01, fit_intercept=False)
            regr.fit(reference_tet, invert_registered_tet)
            strain_matrix = regr.coef_
            reference_tetra_strain_matrix += [strain_matrix]
            reference_tetra_stretch_matrix += [strain_matrix - np.diag(np.ones(3))]

            invert_registered_tetra_base, strain_values, reference_tetra_base = np.linalg.svd(strain_matrix)
            reference_tetra_strain_values += [strain_values]
            reference_tetra_stretch_axes += [reference_tetra_base]
        reference_tetra_strain_matrix = np.array(reference_tetra_strain_matrix)
        reference_tetra_stretch_matrix = np.array(reference_tetra_stretch_matrix)
        reference_tetra_strain_values = np.array(reference_tetra_strain_values)
        reference_tetra_stretch_axes = np.array(reference_tetra_stretch_axes)

        reference_volumetric_growth_values = np.prod(reference_tetra_strain_values, axis=1)
        # reference_volumetric_growth_anisotropy = reference_tetra_strain_values.max(axis=1)/reference_tetra_strain_values.min(axis=1)
        reference_volumetric_growth_anisotropy = np.power((3. / 2.) * np.power(reference_tetra_strain_values - reference_tetra_strain_values.mean(axis=1)[:, np.newaxis], 2).sum(axis=1) / np.power(reference_tetra_strain_values, 2).sum(axis=1), 1 / 2.)
        reference_main_growth_direction = reference_tetra_strain_values[:, 0][:, np.newaxis] * reference_tetra_stretch_axes[:, 0]

        reference_triangulation_topomesh.update_wisp_property('next_strain_tensor', 3, array_dict(reference_tetra_strain_matrix, list(reference_triangulation_topomesh.wisps(3))))
        reference_triangulation_topomesh.update_wisp_property('next_stretch_tensor', 3, array_dict(reference_tetra_stretch_matrix, list(reference_triangulation_topomesh.wisps(3))))
        reference_triangulation_topomesh.update_wisp_property('next_volumetric_growth', 3, array_dict(reference_volumetric_growth_values, list(reference_triangulation_topomesh.wisps(3))))
        reference_triangulation_topomesh.update_wisp_property('next_volumetric_growth_anisotropy', 3, array_dict(reference_volumetric_growth_anisotropy, list(reference_triangulation_topomesh.wisps(3))))
        reference_triangulation_topomesh.update_wisp_property('next_main_growth_direction', 3, array_dict(reference_main_growth_direction, list(reference_triangulation_topomesh.wisps(3))))

        compute_topomesh_vertex_property_from_cells(reference_triangulation_topomesh, 'next_strain_tensor')
        compute_topomesh_vertex_property_from_cells(reference_triangulation_topomesh, 'next_stretch_tensor')
        compute_topomesh_vertex_property_from_cells(reference_triangulation_topomesh, 'next_volumetric_growth')
        compute_topomesh_vertex_property_from_cells(reference_triangulation_topomesh, 'next_volumetric_growth_anisotropy')
        compute_topomesh_vertex_property_from_cells(reference_triangulation_topomesh, 'next_main_growth_direction')

        sequence_triangulations[reference_filename] = reference_triangulation_topomesh

        reference_data['next_strain_tensor'] = list(reference_triangulation_topomesh.wisp_property('next_strain_tensor', 0).values(reference_data.index.values))
        reference_data['next_stretch_tensor'] = list(reference_triangulation_topomesh.wisp_property('next_stretch_tensor', 0).values(reference_data.index.values))
        reference_data['next_volumetric_growth'] = reference_triangulation_topomesh.wisp_property('next_volumetric_growth', 0).values(reference_data.index.values)
        reference_data['next_volumetric_growth_anisotropy'] = reference_triangulation_topomesh.wisp_property('next_volumetric_growth_anisotropy', 0).values(reference_data.index.values)
        reference_data['next_main_growth_direction'] = list(reference_triangulation_topomesh.wisp_property('next_main_growth_direction', 0).values(reference_data.index.values))

        if i_file == 0:
            reference_data['previous_strain_tensor'] = np.nan
            reference_data['previous_stretch_tensor'] = np.nan
            reference_data['previous_volumetric_growth'] = np.nan
            reference_data['previous_volumetric_growth_anisotropy'] = np.nan
            reference_data['previous_main_growth_direction'] = np.nan

        logging.info("".join(["  " for l in range(loglevel)])+"<-- Computing previous volumetric growth "+reference_filename+" <-- "+floating_filename+" ["+str(current_time()-start_time)+" s]")

        X = floating_data['center_x']
        Y = floating_data['center_y']
        Z = floating_data['center_z']

        floating_points = np.transpose([X, Y, Z])
        floating_positions = array_dict(floating_points, floating_data.index.values)

        homogeneous_points = np.concatenate([microscope_orientation * np.transpose([X, Y, Z]), np.ones((len(X), 1))], axis=1)
        transformed_points = np.einsum("...ij,...j->...i", invert_rigid_matrix, homogeneous_points)
        transformed_points = microscope_orientation * transformed_points[:, :3]

        previous_transform = np.dot(invert_rigid_matrix, previous_transform)
        sequence_transformed_points = np.einsum("...ij,...j->...i", previous_transform, homogeneous_points)
        sequence_transformed_points = microscope_orientation * sequence_transformed_points[:, :3]

        floating_data['transformed_x'] = transformed_points[:, 0]
        floating_data['transformed_y'] = transformed_points[:, 1]
        floating_data['transformed_z'] = transformed_points[:, 2]

        transformed_positions = array_dict(transformed_points, floating_data.index.values)

        if not floating_filename in sequence_triangulations.keys():
            floating_triangulation_topomesh = nuclei_tetrahedrization_topomesh(floating_data, maximal_distance=maximal_length)
        else:
            floating_triangulation_topomesh = sequence_triangulations[floating_filename]

        transform_triangulation_topomesh = deepcopy(floating_triangulation_topomesh)
        transform_triangulation_topomesh.update_wisp_property('barycenter', 0, transformed_positions)

        compute_topomesh_property(transform_triangulation_topomesh, 'vertices', 3)
        floating_tetras = transform_triangulation_topomesh.wisp_property('vertices', 3).values(list(transform_triangulation_topomesh.wisps(3)))
        transform_tetra_points = transformed_positions.values(floating_tetras)
        transform_tetra_volumes = tetra_geometric_features(floating_tetras, transformed_positions, ['volume'])[:, 0]
        transform_triangulation_topomesh.update_wisp_property('volume', 3, array_dict(transform_tetra_volumes, list(transform_triangulation_topomesh.wisps(3))))

        registered_triangulation_topomesh = deepcopy(transform_triangulation_topomesh)

        image_coords = tuple(np.transpose(np.minimum(size - 1, np.maximum(0, (transformed_points / resolution).astype(int)))))
        point_displacement = vector_field[image_coords]

        for i_dim, dim in enumerate(['x', 'y', 'z']):
            if "previous_motion_" + dim in floating_data.columns:
                del floating_data['previous_motion_' + dim]
            floating_data['previous_motion_' + dim] = point_displacement[:, i_dim]
            if i_file == len(filenames) - 2:
                if 'next_motion_' + dim in floating_data.columns:
                    del floating_data['next_motion_' + dim]
                floating_data['next_motion_' + dim] = 0

        floating_data['previous_motion_x'] = point_displacement[:, 0]
        floating_data['previous_motion_y'] = point_displacement[:, 1]
        floating_data['previous_motion_z'] = point_displacement[:, 2]

        if i_file == len(filenames) - 2:
            floating_data['next_motion_x'] = 0
            floating_data['next_motion_y'] = 0
            floating_data['next_motion_z'] = 0

        registered_points = transformed_points + point_displacement
        registered_positions = array_dict(registered_points, floating_data.index.values)
        registered_tetra_points = registered_positions.values(floating_tetras)

        registered_triangulation_topomesh.update_wisp_property('barycenter', 0, registered_positions)

        registered_tetra_volumes = tetra_geometric_features(floating_tetras, registered_positions, ['volume'])[:, 0]
        registered_triangulation_topomesh.update_wisp_property('volume', 3, array_dict(registered_tetra_volumes, list(transform_triangulation_topomesh.wisps(3))))

        floating_volumetric_growth_values = transform_tetra_volumes / registered_tetra_volumes

        transform_centered_tetra_points = transform_tetra_points - np.mean(transform_tetra_points, axis=1)[:, np.newaxis]
        registered_centered_tetra_points = registered_tetra_points - np.mean(registered_tetra_points, axis=1)[:, np.newaxis]

        transform_tetra_strain_matrix = []
        transform_tetra_stretch_matrix = []
        transform_tetra_strain_values = []
        transform_tetra_stretch_axes = []
        for transform_tet, registered_tet in zip(transform_centered_tetra_points, registered_centered_tetra_points):
            regr = linear_model.Ridge(alpha=.01, fit_intercept=False)
            # regr.fit(transform_tet, registered_tet)
            regr.fit(registered_tet, transform_tet)
            strain_matrix = regr.coef_

            transform_tetra_strain_matrix += [strain_matrix]
            transform_tetra_stretch_matrix += [strain_matrix - np.diag(np.ones(3))]

            registered_tetra_base, strain_values, transform_tetra_base = np.linalg.svd(strain_matrix)
            transform_tetra_strain_values += [strain_values]
            transform_tetra_stretch_axes += [transform_tetra_base]
        transform_tetra_strain_matrix = np.array(transform_tetra_strain_matrix)
        transform_tetra_stretch_matrix = np.array(transform_tetra_stretch_matrix)
        transform_tetra_strain_values = np.array(transform_tetra_strain_values)
        transform_tetra_stretch_axes = np.array(transform_tetra_stretch_axes)

        transform_volumetric_growth_values = np.prod(transform_tetra_strain_values, axis=1)
        # transform_volumetric_growth_anisotropy = transform_tetra_strain_values.max(axis=1)/transform_tetra_strain_values.min(axis=1)
        transform_volumetric_growth_anisotropy = np.power((3. / 2.) * np.power(transform_tetra_strain_values - transform_tetra_strain_values.mean(axis=1)[:, np.newaxis], 2).sum(axis=1) / np.power(transform_tetra_strain_values, 2).sum(axis=1), 1 / 2.)
        transform_main_growth_direction = transform_tetra_strain_values[:, 0][:, np.newaxis] * transform_tetra_stretch_axes[:, 0]

        transform_triangulation_topomesh.update_wisp_property('next_strain_tensor', 3, array_dict(transform_tetra_strain_matrix, list(transform_triangulation_topomesh.wisps(3))))
        transform_triangulation_topomesh.update_wisp_property('next_stretch_tensor', 3, array_dict(transform_tetra_stretch_matrix, list(transform_triangulation_topomesh.wisps(3))))
        transform_triangulation_topomesh.update_wisp_property('next_volumetric_growth', 3, array_dict(transform_volumetric_growth_values, list(transform_triangulation_topomesh.wisps(3))))
        transform_triangulation_topomesh.update_wisp_property('next_volumetric_growth_anisotropy', 3, array_dict(transform_volumetric_growth_anisotropy, list(transform_triangulation_topomesh.wisps(3))))
        transform_triangulation_topomesh.update_wisp_property('next_main_growth_direction', 3, array_dict(transform_main_growth_direction, list(transform_triangulation_topomesh.wisps(3))))

        compute_topomesh_vertex_property_from_cells(transform_triangulation_topomesh, 'next_strain_tensor')
        compute_topomesh_vertex_property_from_cells(transform_triangulation_topomesh, 'next_stretch_tensor')
        compute_topomesh_vertex_property_from_cells(transform_triangulation_topomesh, 'next_volumetric_growth')
        compute_topomesh_vertex_property_from_cells(transform_triangulation_topomesh, 'next_volumetric_growth_anisotropy')
        compute_topomesh_vertex_property_from_cells(transform_triangulation_topomesh, 'next_main_growth_direction')

        sequence_triangulations[floating_filename] = transform_triangulation_topomesh

        floating_data['previous_strain_tensor'] = list(transform_triangulation_topomesh.wisp_property('next_strain_tensor', 0).values(floating_data.index.values))
        floating_data['previous_stretch_tensor'] = list(transform_triangulation_topomesh.wisp_property('next_stretch_tensor', 0).values(floating_data.index.values))
        floating_data['previous_volumetric_growth'] = transform_triangulation_topomesh.wisp_property('next_volumetric_growth', 0).values(floating_data.index.values)
        floating_data['previous_volumetric_growth_anisotropy'] = transform_triangulation_topomesh.wisp_property('next_volumetric_growth_anisotropy', 0).values(floating_data.index.values)
        floating_data['previous_main_growth_direction'] = list(transform_triangulation_topomesh.wisp_property('next_main_growth_direction', 0).values(floating_data.index.values))


        if i_file == len(filenames) - 2:
            floating_data['next_strain_tensor'] = np.nan
            floating_data['next_stretch_tensor'] = np.nan
            floating_data['next_volumetric_growth'] = np.nan
            floating_data['next_volumetric_growth_anisotropy'] = np.nan
            floating_data['next_main_growth_direction'] = np.nan

        # floating_X = floating_data['transformed_x'].values
        # floating_Y = floating_data['transformed_y'].values

        # reference_next_X = invert_registered_points[:,0]
        # reference_next_Y = invert_registered_points[:,1]

        # variable_names = ['DIIV','Normalized_DIIV','Auxin','Normalized Auxin','DR5','Normalized_DR5','CLV3','TagBFP','Normalized_TagBFP','mean_curvature','gaussian_curvature']

        # for var in variable_names:
        #     if (var in reference_data.columns) and (var in floating_data.columns):
        #         floating_var = floating_data[var].values
        #         reference_next_var = compute_local_2d_signal(np.transpose([floating_X,floating_Y]),np.transpose([reference_next_X,reference_next_Y]),floating_var)
        #         reference_data['next_'+var] = reference_next_var
        #     if i_file == len(filenames)-2:
        #         floating_data['next_'+var] = np.nan

        # reference_X = reference_data['center_x'].values
        # reference_Y = reference_data['center_y'].values

        # floating_previous_X = registered_points[:,0]
        # floating_previous_Y = registered_points[:,1]

        # for var in variable_names:
        #     if (var in reference_data.columns) and (var in floating_data.columns):
        #         reference_var = reference_data[var].values
        #         floating_previous_var = compute_local_2d_signal(np.transpose([reference_X,reference_Y]),np.transpose([floating_previous_X,floating_previous_Y]),reference_var)
        #         floating_data['previous_'+var] = floating_previous_var
        #     if i_file == 0:
        #         reference_data['previous_'+var] = np.nan

    return sequence_data, sequence_triangulations
