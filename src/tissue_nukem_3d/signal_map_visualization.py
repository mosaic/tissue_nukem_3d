import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.patches as patch
from matplotlib import cm
from matplotlib.colors import Normalize


def plot_tensor_data(figure, X, Y, tensors, C=None, tensor_style='crosshair', colormap='jet', value_range=None, scale=10., linewidth=1, alpha=1.):
    if C is None:
        C = np.trace(tensors, axis1=1, axis2=2)

    if value_range is None:
        value_range = (np.nanmin(C), np.nanmax(C))

    lut = cm.ScalarMappable(cmap=colormap, norm=Normalize(vmin=value_range[0], vmax=value_range[1]))

    for x, y, t, c in zip(X, Y, tensors, C):
        if not np.any(np.isnan(t)):
            color = lut.to_rgba(c)

            vals, vecs = np.linalg.eigh(t)
            order = vals.argsort()[::-1]
            vals = np.maximum(0, vals)[order]
            vecs = vecs[order]

            if tensor_style == 'crosshair':
                figure.gca().plot([x - scale * vals[0] * vecs[0, 0], x + scale * vals[0] * vecs[0, 0]],
                                  [y - scale * vals[0] * vecs[0, 1], y + scale * vals[0] * vecs[0, 1]], color=color, linewidth=linewidth, alpha=alpha, zorder=8)
                figure.gca().plot([x - scale * vals[1] * vecs[1, 0], x + scale * vals[1] * vecs[1, 0]],
                                  [y - scale * vals[1] * vecs[1, 1], y + scale * vals[1] * vecs[1, 1]], color=color, linewidth=linewidth / 2., alpha=alpha, zorder=8)

            elif tensor_style == 'ellipse':
                theta = np.degrees(np.arctan2(*vecs[0, :2][::-1]))
                # width, height = 2. * scale * np.sqrt(np.maximum(0,vals)[:2])
                width, height = 2 * scale * np.maximum(0, vals)[:2]
                ellipse = patch.Ellipse(xy=[x, y], width=width, height=height, angle=theta, alpha=alpha, color=color)
                figure.gca().add_artist(ellipse)


def plot_vector_data(figure, X, Y, vectors, C=None, vector_style='line', colormap='jet', value_range=None, scale=10., linewidth=1, alpha=1.):
    if C is None:
        C = np.linalg.norm(vectors, axis=-1)

    if value_range is None:
        value_range = (np.nanmin(C), np.nanmax(C))

    lut = cm.ScalarMappable(cmap=colormap, norm=Normalize(vmin=value_range[0], vmax=value_range[1]))

    for x, y, v, c in zip(X, Y, vectors, C):
        if not np.any(np.isnan(v)):
            color = lut.to_rgba(c)

            if vector_style == 'line':
                figure.gca().plot([x - scale * v[0], x + scale * v[0]],
                                  [y - scale * v[1], y + scale * v[0]], color=color, linewidth=linewidth, alpha=alpha, zorder=8)


def plot_signal_map(signal_map, signal_name, figure, colormap="Greys_r", signal_range=None, signal_lut_range=None, distance_rings=True, alpha=1., subsample=4, scale=1., color_name=None):
    if signal_lut_range is None:
        signal_lut_range = (np.nanmin(signal_map.signal_map(signal_name)), np.nanmax(signal_map.signal_map(signal_name)))

    if signal_map.signal_map(signal_name).ndim == 2:

        if signal_range is None:
            if signal_name in signal_map.signal_data.columns:
                signal_range = (np.nanmin(signal_map.signal_data[signal_name].values), np.nanmax(signal_map.signal_data[signal_name].values))
            else:
                signal_range = (np.nanmin(signal_map.signal_map(signal_name)), np.nanmax(signal_map.signal_map(signal_name)))

        # col = figure.gca().contourf(signal_map.xx,signal_map.yy,signal_map.signal_map(signal_name),np.linspace(signal_range[0],signal_range[1],51),cmap=colormap,alpha=1,antialiased=True,vmin=signal_lut_range[0],vmax=signal_lut_range[1],extend='both')
        col = figure.gca().pcolormesh(signal_map.xx, signal_map.yy, signal_map.signal_map(signal_name), cmap=colormap, shading='gouraud', antialiased=True, vmin=signal_lut_range[0], vmax=signal_lut_range[1],alpha=alpha)
        figure.gca().contour(signal_map.xx, signal_map.yy, signal_map.signal_map(signal_name), np.linspace(signal_range[0], signal_range[1], 51), cmap='gray', alpha=0.2*alpha, linewidths=1, antialiased=True, vmin=-1, vmax=0)
    elif signal_map.signal_map(signal_name).ndim >= 3:

        sub_map_x = np.concatenate(signal_map.xx[::subsample, ::subsample])
        sub_map_y = np.concatenate(signal_map.yy[::subsample, ::subsample])
        sub_map_confidence = np.concatenate(signal_map.confidence_map[::subsample, ::subsample])

        sub_map_x = sub_map_x[sub_map_confidence > 0.5]
        sub_map_y = sub_map_y[sub_map_confidence > 0.5]
        
        if color_name is not None:
            sub_map_colors = np.concatenate(signal_map.signal_map(color_name)[::subsample, ::subsample])
            sub_map_colors = sub_map_colors[sub_map_confidence > 0.5]
        else:
            sub_map_colors = None
        
        if signal_map.signal_map(signal_name).ndim == 3:
            sub_map_vectors = np.concatenate(signal_map.signal_map(signal_name)[::subsample, ::subsample])
            sub_map_vectors = sub_map_vectors[sub_map_confidence > 0.5]
            plot_vector_data(figure, sub_map_x, sub_map_y, sub_map_vectors, sub_map_colors, colormap=colormap, value_range=signal_lut_range, vector_style='line', scale=scale*subsample/2., alpha=alpha)
        elif signal_map.signal_map(signal_name).ndim == 4:
            sub_map_tensors = np.concatenate(signal_map.signal_map(signal_name)[::subsample, ::subsample])
            sub_map_tensors = sub_map_tensors[sub_map_confidence > 0.5]
            plot_tensor_data(figure, sub_map_x, sub_map_y, sub_map_tensors, sub_map_colors, colormap=colormap, value_range=signal_lut_range, tensor_style='crosshair', scale=2*scale*subsample,  alpha=alpha)
            plot_tensor_data(figure, sub_map_x, sub_map_y, sub_map_tensors, sub_map_colors, colormap=colormap, value_range=signal_lut_range, tensor_style='ellipse', scale=2*scale*subsample, alpha=alpha/5.)

        col = None

    for a in range(16):
        # background = np.zeros_like(signal_map.confidence_map)
        # background[signal_map.confidence_map>0.1+a/24.] = np.nan
        # figure.gca().pcolormesh(signal_map.xx,signal_map.yy,background,cmap='gray_r', antialiased=True,alpha=1-a/15.,vmin=1,vmax=2)
        figure.gca().contourf(signal_map.xx, signal_map.yy, signal_map.confidence_map, [-100, 0.1 + a / 24.], cmap='gray_r', alpha=1 - a / 15., vmin=1, vmax=2)

    figure.gca().plot([signal_map.xx.min(), signal_map.xx.min()], [signal_map.yy.min(), signal_map.yy.max()], color='w', linewidth=2)
    figure.gca().plot([signal_map.xx.min(), signal_map.xx.max()], [signal_map.yy.min(), signal_map.yy.min()], color='w', linewidth=2)
    figure.gca().plot([signal_map.xx.min(), signal_map.xx.max()], [signal_map.yy.max(), signal_map.yy.max()], color='w', linewidth=2)
    figure.gca().plot([signal_map.xx.max(), signal_map.xx.max()], [signal_map.yy.min(), signal_map.yy.max()], color='w', linewidth=2)
    # c = patch.Circle(xy=[0,0],radius=clv3_radius,ec="#c94389",fc='None',lw=5,alpha=0.5)
    # figure.gca().add_artist(c)

    if distance_rings:
        CS = figure.gca().contour(signal_map.xx, signal_map.yy, signal_map.rr, np.linspace(0, signal_map.extent, signal_map.extent / 10. + 1), cmap='Greys', vmin=-1, vmax=0, linewidth=1, alpha=0.2)
        figure.gca().clabel(CS, inline=1, fontsize=8, alpha=0.2)

    figure.gca().axis('equal')
    figure.gca().set_xlim(-signal_map.extent * 1.05, signal_map.extent * 1.05)
    figure.gca().set_ylim(-signal_map.extent * 1.05, signal_map.extent * 1.05)
    figure.gca().axis('off')

    return col
