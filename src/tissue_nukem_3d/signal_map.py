import re
import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd
import pandas as pd


from tissue_nukem_3d.epidermal_maps import nuclei_density_function

import warnings

class SignalMap(object):

    def __init__(self, signal_data, extent, origin=np.array([0,0]), resolution=1, position_name='center', radius=7.5, density_k=0.55, polar=False):
        self.signal_data = signal_data
        self.extent = extent
        self.origin = origin
        self.resolution = resolution
        self.position_name = position_name
        self.radius = radius
        self.density_k = density_k
        self.polar = polar

        self.xx = None
        self.yy = None
        self.rr = None
        self.tt = None
        self.confidence_map = None

        self.potential = None
        self.signal_maps = {}

        self.compute_map_grid()
        self.compute_potential(position_name=self.position_name,radius=self.radius,density_k=self.density_k)

    def __getitem__(self, signal_name):
        return self.signal_map(signal_name)

    @property
    def shape(self):
        return self.xx.shape


    def signal_names(self):
        return [s for s in self.signal_maps.keys() if s in self.signal_data.columns]


    def compute_map_grid(self, recompute=False):
        if not self.polar:
            if self.xx is None or self.yy is None or recompute:
                x_range = np.linspace(self.origin[0]-self.extent,self.origin[0]+self.extent,2*self.extent/self.resolution+1)
                y_range = np.linspace(self.origin[1]-self.extent,self.origin[1]+self.extent,2*self.extent/self.resolution+1)
                self.xx, self.yy = np.meshgrid(x_range,y_range)
            self.rr = np.linalg.norm([self.xx,self.yy],axis=0)
            self.tt = np.sign(self.yy)*np.arccos(self.xx/self.rr)
            self.tt[(self.rr==0)] = 0.
            self.tt[(self.yy==0)&(self.xx<0)] = np.pi
        else:
            if self.rr is None or self.tt is None or recompute:
                r_range = np.linspace(0,self.extent,self.extent/self.resolution+1)
                t_range = np.radians(np.linspace(-180.,180.,180./self.resolution+1))
                self.rr, self.tt = np.meshgrid(r_range,t_range)
            self.xx = self.rr*np.cos(self.tt)
            self.yy = self.rr*np.sin(self.tt)

        self.confidence_map = np.zeros_like(self.xx)


    def compute_potential(self, position_name=None, radius=None, density_k=None):
        if len(self.signal_data):
            if position_name is not None:
                self.position_name = position_name
            if radius is not None:
                self.radius = radius
            if density_k is not None:
                self.density_k = density_k

            X = self.signal_data[self.position_name+'_x'].values
            Y = self.signal_data[self.position_name+'_y'].values
            projected_positions = dict(list(zip(list(range(len(X))),np.transpose([X,Y,np.zeros_like(X)]))))

            potential = np.array([nuclei_density_function(dict([(p,projected_positions[p])]),cell_radius=self.radius,k=self.density_k)(self.xx,self.yy,np.zeros_like(self.xx)) for p in range(len(X))])
            self.potential = np.transpose(potential,(1,2,0))
            # density = np.sum(potential,axis=-1)

            self.confidence_map = np.sum(self.potential,axis=-1)
            # if np.all(self.origin==0):
            #     self.confidence_map += np.maximum(1-np.linalg.norm([self.xx,self.yy],axis=0)/(self.extent/2.),0)
            self.confidence_map = nd.gaussian_filter(self.confidence_map,sigma=1.0/self.resolution)
    

    def signal_map(self, signal_name):
        if not signal_name in self.signal_maps:
            if not signal_name in self.signal_data.columns:
                raise KeyError("The map for \""+signal_name+"\" is not defined!")
            else:
                self.compute_signal_map(signal_name)
        return self.signal_maps[signal_name]


    def compute_signal_map(self, signal_name):
        if not signal_name in self.signal_data.columns:
            raise KeyError("The signal \""+signal_name+"\" is not defined in the data!")
        else:
            signal_values = self.signal_data[signal_name].values
            if np.iterable(signal_values[0]):
                signal_values = np.array([list(v) for v in signal_values])
            mask = np.logical_not(np.any([np.isnan(signal_values),np.isinf(signal_values)],axis=0))
            while mask.ndim>1:
                mask = np.any(mask,axis=-1)
            print(signal_values.ndim, signal_values.shape)
            if signal_values.ndim == 1:
                signal_map = np.sum(self.potential[:,:,mask]*signal_values[mask][np.newaxis,np.newaxis,:],axis=-1)/np.sum(self.potential[:,:,mask],axis=-1)
            elif signal_values.ndim == 2:
                signal_map = np.sum(self.potential[:, :, mask,np.newaxis] * signal_values[mask][np.newaxis, np.newaxis, :], axis=-2) / np.sum(self.potential[:, :, mask], axis=-1)[:,:,np.newaxis]
            elif signal_values.ndim == 3:
                signal_map = np.sum(self.potential[:, :, mask,np.newaxis,np.newaxis] * signal_values[mask][np.newaxis, np.newaxis, :], axis=-3) / np.sum(self.potential[:, :, mask], axis=-1)[:,:,np.newaxis,np.newaxis]

            self.signal_maps[signal_name] = signal_map


    def update_signal_map(self, signal_name, signal_map):
        if not signal_map.shape == self.shape:
            raise IndexError("The map passed as argument for \""+signal_name+"\" has a different shape!")
        else:
            if signal_name in self.signal_maps:
                warnings.warn("The map for \""+signal_name+"\" already exists, it will be owerwritten")
            self.signal_maps[signal_name] = signal_map

    def map_grid(self, polar=False):
        if not polar:
            return self.xx, self.yy
        else:
            return self.rr, self.tt


def save_signal_map(signal_map, map_filename, signal_names=None):
    start_time = current_time()
    print("--> Saving .map")

    map_file = open(map_filename,'w+')

    map_file.write("map\n")
    map_file.write("format ascii 1.0\n")

    map_file.write("property polar "+str(signal_map.polar)+"\n")
    map_file.write("shape "+str(signal_map.shape[0])+" "+str(signal_map.shape[1])+"\n")


    if not signal_map.polar:
        map_file.write("array xx\n")
        map_file.write("array yy\n")
    else:
        map_file.write("array rr\n")
        map_file.write("array tt\n")
    map_file.write("array confidence\n")

    if signal_names is None:
        signal_names = signal_map.signal_names()

    for signal_name in signal_names:
        if signal_map.signal_map(signal_name).ndim == 2:
            map_file.write("array "+signal_name+"\n")
        elif signal_map.signal_map(signal_name).ndim == 3:
            map_file.write("vectorfield "+str(signal_map.signal_map(signal_name).shape[2])+" "+signal_name+"\n")

    map_file.write("end_header\n")

    if not signal_map.polar:
        signal_map.xx.tofile(map_file,sep=" ")
        map_file.write("\n")
        signal_map.yy.tofile(map_file,sep=" ")
        map_file.write("\n")
    else:
        signal_map.rr.tofile(map_file,sep=" ")
        map_file.write("\n")
        signal_map.tt.tofile(map_file,sep=" ")
        map_file.write("\n")
    signal_map.confidence_map.tofile(map_file,sep=" ")
    map_file.write("\n")

    for signal_name in signal_names:
        signal_map.signal_map(signal_name).tofile(map_file,sep=" ")
        map_file.write("\n")

    map_file.flush()
    map_file.close()

    end_time = current_time()
    print("<-- Saving .map        [", end_time - start_time, "s]")


def read_signal_map(map_filename):
    start_time = current_time()
    print("--> Reading .map")

    map_file = open(map_filename,'rU')
    map_stream = enumerate(map_file,1)
    assert "map" in map_stream.__next__()[1]
    assert "ascii" in map_stream.__next__()[1]

    polar = False
    shape = (1,1)
    array_names = []
    vectorfield_shapes = {}
    lineno, line = next(map_stream)
    while not 'end_header' in line:
        try:
            if re.split(' ',line)[0] == 'shape':
                shape = tuple([int(s) for s in re.split(' ',line)[1:]])

            if re.split(' ',line)[0] == 'property':
                property_name = re.split(' ',line)[1]
                if property_name == 'polar':
                    polar = eval(re.split(' ',line)[-1][:-1])

            if re.split(' ', line)[0] == 'array':
                array_name = re.split(' ',line)[1][:-1]
                array_names += [array_name]
            if re.split(' ', line)[0] == 'vectorfield':
                vectorfield_shape = int(re.split(' ',line)[1])
                vectorfield_name = re.split(' ',line)[2][:-1]
                array_names += [vectorfield_name]
                vectorfield_shapes[vectorfield_name] = vectorfield_shape

        except Exception as e:
            raise ValueError(map_filename, lineno, line, e)

        lineno, line = next(map_stream)

    print((array_names, shape, polar))

    signal_data = pd.DataFrame(columns=[c for c in array_names if c not in ['xx','yy','rr','tt','confidence']])
    signal_map = SignalMap(signal_data, 0, polar=polar)

    map_coords = 0
    for array_name in array_names:
        lineno, line = next(map_stream)

        try:
            print(array_name, lineno, map_coords, signal_map.polar, array_name=='xx')
            if not signal_map.polar:
                if array_name == 'xx':
                    signal_map.xx = np.fromstring(line,sep=' ').reshape(shape)
                    map_coords += 1

                if array_name == 'yy':
                    signal_map.yy = np.fromstring(line,sep=' ').reshape(shape)
                    map_coords += 1
            else:
                if array_name == 'rr':
                    signal_map.rr = np.fromstring(line,sep=' ').reshape(shape)
                    map_coords += 1

                if array_name == 'tt':
                    signal_map.tt = np.fromstring(line,sep=' ').reshape(shape)
                    map_coords += 1
            if map_coords == 2:
                print((signal_map.xx.shape, signal_map.rr.shape))
                signal_map.compute_map_grid()
                print((signal_map.xx.shape, signal_map.rr.shape))
                if not signal_map.polar:
                    signal_map.resolution = signal_map.xx[1,1] - signal_map.xx[0,0]
                    signal_map.extent = (signal_map.shape[0]-1)*signal_map.resolution/2.
                    print((signal_map.resolution, signal_map.extent))
                else:
                    signal_map.resolution = signal_map.rr[1,1] - signal_map.rr[0,0]
                    signal_map.extent = (signal_map.shape[1]-1)*signal_map.resolution
                    print((signal_map.resolution, signal_map.extent))
                map_coords = 4

            if array_name not in ['xx','yy','rr','tt']:

                if array_name == 'confidence':
                    signal_map.confidence_map = np.fromstring(line,sep=' ').reshape(shape)
                else:
                    if array_name in vectorfield_shapes.keys():
                        signal_map.signal_maps[array_name] = np.fromstring(line,sep=' ').reshape(shape+(vectorfield_shapes[array_name],))
                    else:
                        signal_map.signal_maps[array_name] = np.fromstring(line,sep=' ').reshape(shape)

        except Exception as e:
            raise ValueError(map_filename, lineno, line, e)

    map_file.close()

    signal_map.signal_data = pd.DataFrame(index=['min', 'max'], columns=[c for c in array_names if c not in ['xx', 'yy', 'rr', 'tt', 'confidence']])

    for c in signal_map.signal_data.columns:
        signal_map.signal_data[c].loc['min'] = np.nanmin(signal_map.signal_maps[c])
        signal_map.signal_data[c].loc['max'] = np.nanmax(signal_map.signal_maps[c])

    end_time = current_time()
    print("<--Reading .map        [", end_time - start_time, "s]")

    return signal_map




