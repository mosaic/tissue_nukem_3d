# -*- coding: utf-8 -*-
# -*- python -*-
#
#       TissueLab
#
#       Copyright 2015 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#       File contributor(s):
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       TissueLab Website : http://virtualplants.github.io/
#
###############################################################################

import tissue_nukem_3d
from tissue_nukem_3d.utils.colormap.colormap_utils import Colormap, colormap_from_file

import os
import re

colormaps_path = tissue_nukem_3d.__path__[0]+"/../../../share/data/colormaps/"

def list_colormaps():
    colormap_names = []

    for colormap_file in [f for f in os.listdir(colormaps_path) if re.match(r".*.lut",f)]:
        colormap_name = colormap_file[:-4]
        colormap_names.append(colormap_name)
    colormap_names.sort()
    return colormap_names


def load_colormaps():
    colormaps = {}

    for colormap_file in [f for f in os.listdir(colormaps_path) if re.match(r".*.lut",f)]:
        colormap_name = colormap_file[:-4]
        colormaps[colormap_name] = colormap_from_file(colormaps_path+"/"+colormap_file, name=colormap_name)
    return colormaps
