"""
An library for the detection and quantitative data extraction from microscopy images of cell nuclei
"""
# {# pkglts, base

from . import version

__version__ = version.__version__

# #}
