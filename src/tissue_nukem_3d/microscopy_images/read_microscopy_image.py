 
import numpy as np

from timagetk.components import SpatialImage
from timagetk.io import imread, imsave

import os

def read_lsm_image(lsm_file, channel_names=None):
    from . import lsmreader

    lsm_img = lsmreader.Lsmimage(lsm_file)
    lsm_img.open()

    voxelsize = tuple([float(np.around(lsm_img.header['CZ LSM info']['Voxel Size '+dim]*1000000,decimals=3)) for dim in ['X','Y','Z']])
    n_channels = len(lsm_img.image['data'])

    filename = lsm_file.split('/')[-1]
    print(filename," : ",n_channels," Channels ",voxelsize)

    if n_channels > 1:
        if channel_names is None:
            channel_names = ["CH"+str(i) for i in range(n_channels)]
        img = {}
        for i_channel,channel_name in enumerate(channel_names):
            img[channel_name] = SpatialImage(lsm_img.image['data'][i_channel],voxelsize=voxelsize)
    else:
        img = SpatialImage(lsm_img.image['data'][0],voxelsize=voxelsize)

    return img


def read_czi_image(czi_file, channel_names=None, pattern=".C.ZXY."):
    from .czifile import CziFile

    czi_img = CziFile(czi_file)
    czi_channels = czi_img.asarray()

    metadata_pattern = None

    metadata_voxelsize = {}
    metadata_channel_names = []
    metadata_channel_fluos = []
    for s in czi_img.segments():
        if s.SID.decode() == "ZISRAWSUBBLOCK":
            metadata = str(s).split('\n')

            for i,row in enumerate(metadata):
                if 'DirectoryEntry' in row:
                    next = metadata[i+1]
                    metadata_pattern = next.split(' ')[4]
                    if metadata_pattern[:2] == 'b\'': #decoding issue?
                        metadata_pattern = metadata_pattern[2:-1]

        if s.SID.decode() == "ZISRAWMETADATA":
            metadata = s.data().split('\n')

            for i,row in enumerate(metadata):
                if "Distance Id" in row:
                    next = metadata[i+1]
                    metadata_voxelsize[row.split('"')[1]] = np.around(float(next[next.find('>')+1:next.find('>')+next[next.find('>'):].find('<')])*1e6,decimals=3)

                if "ChannelName" in row:
                    next = metadata[i]
                    metadata_channel_names += [next[next.find('>')+1:next.find('>')+next[next.find('>'):].find('<')]]

                if "<Fluor>" in row:
                    next = metadata[i]
                    metadata_channel_fluos += [next[next.find('>')+1:next.find('>')+next[next.find('>'):].find('<')]]

    if metadata_pattern is not None and len(metadata_pattern)==czi_channels.ndim and np.all([metadata_pattern.find(d) != -1 for d in 'CXYZ']):
        pattern = metadata_pattern

    czi_channels = np.transpose(czi_channels,tuple([pattern.find(c) for c in 'CXYZ']+[i for i in range(len(czi_channels.shape)) if not pattern[i] in 'CXYZ']))
    czi_channels = czi_channels.reshape(czi_channels.shape[:4])
    print(czi_channels.shape)

    voxelsize = tuple([metadata_voxelsize[dim] for dim in ['X','Y','Z']])
    n_channels = czi_channels.shape[0]

    print(czi_file.split('/')[-1]," : ",n_channels," Channels ",voxelsize)

    if n_channels > 1:
        if channel_names is None:
            channel_names = [n+"_"+f for n,f in zip(metadata_channel_names,metadata_channel_fluos)]
            # channel_names = ["CH"+str(i) for i in range(n_channels)]
        img = {}
        for i_channel,channel_name in enumerate(channel_names):
            img[channel_name] = SpatialImage(czi_channels[i_channel],voxelsize=voxelsize)
    else:
        img = SpatialImage(czi_channels[0],voxelsize=voxelsize)

    return img


def read_tiff_image(tiff_file, channel_names=None, pattern="ZCXY", voxelsize=None):
    from .tifffile import TiffFile

    tiff_img = TiffFile(tiff_file)
    tiff_channels = tiff_img.asarray()
    if tiff_channels.shape[0] == 1:
        tiff_channels = tiff_channels[0]

    metadata_dict = {}
    int_tags = ['x_resolution', 'y_resolution', 'image_width', 'image_length', 'image_description', 'location']
    for page in tiff_img:
        for tag in page.tags.values():
            if tag.name in int_tags:
                if 'resolution' in tag.name:
                    if (isinstance(tag.value, tuple) and len(tag.value)==2):
                        res = float(tag.value[0]/tag.value[1])  
                        metadata_dict[tag.name] = res
                    elif isinstance(tag.value, float):
                        #--- todo float management ?
                        pass
                else:
                    metadata_dict[str(tag.name)] = tag.value

    if 'image_description' in metadata_dict:
        description = metadata_dict['image_description'].split('\n')
        metadata_dict.update(dict(x.split('=') for x in description if len(x.split('='))==2))
        del metadata_dict['image_description']
    # print metadata_dict

    if voxelsize is None:
        voxelsize = [1.,1.,1.]
        if 'x_resolution' in metadata_dict:
            voxelsize[0] = 1./float(metadata_dict['x_resolution'])
        if 'y_resolution' in metadata_dict:
            voxelsize[1] = 1./float(metadata_dict['y_resolution'])
        if 'spacing' in metadata_dict:
            voxelsize[2] = float(metadata_dict['spacing'])
        voxelsize = tuple(voxelsize)
    # print voxelsize

    n_channels = 1 if tiff_channels.ndim==3 else tiff_channels.shape[pattern.find('C')]

    if n_channels > 1:
        tiff_channels = np.transpose(tiff_channels,tuple([pattern.find(c) for c in 'CXYZ']))
        if channel_names is None:
            channel_names = ["CH"+str(i) for i in range(n_channels)]
        img = {}
        for i_channel,channel_name in enumerate(channel_names):
            if channel_name is not None:
                img[channel_name] = SpatialImage(tiff_channels[i_channel],voxelsize=voxelsize)
        if len(img) == 1:
            img = img.values()[0]
    else:
        if tiff_channels.ndim>3:
            tiff_channels = tiff_channels[tuple([slice(0, tiff_channels.shape[d]) if (d != pattern.find('C')) else 0 for d in range(tiff_channels.ndim)])]
        pattern = pattern.replace('C','')
        img = SpatialImage(np.transpose(tiff_channels,tuple([pattern.find(c) for c in 'XYZ'])),voxelsize=voxelsize)

    return img


def save_tiff_image(tiff_file, img_dict, pattern="ZCYX"):
    from .tifffile import TiffWriter

    POSS_EXT = ['.tif', '.tiff']

    (filepath, filename) = os.path.split(tiff_file)
    (shortname, ext) = os.path.splitext(filename)
    try:
        assert ext in POSS_EXT
    except AssertionError:
        raise NotImplementedError(
            "Unknown file ext '{}', should be in {}.".format(ext, POSS_EXT))

    with TiffWriter(tiff_file, bigtiff=False, imagej=True) as tif:
        channel_names = list(img_dict.keys())
        n_channels = len(channel_names)
        voxelsize = img_dict[channel_names[0]].voxelsize
        n_slices = img_dict[channel_names[0]].shape[2]
        data = np.array([np.transpose(img_dict[c].get_array(), (2, 1, 0)) for c in channel_names])
        data = np.transpose(data,tuple([pattern.find(c) for c in 'CZYX']))
        # print data.shape
        metadata_dict={'spacing': voxelsize[2], 'slices':n_slices, 'channels':n_channels}
        # for i_c,channel_name in enumerate(channel_names):
            # metadata_dict['Information|Image|Channel|Name #'+str(i_c+1)] = channel_name
        tif.save(data, compress=0, resolution=(1.0/voxelsize[0],1.0/voxelsize[1]), metadata=metadata_dict)
    tif.close()
    return


def export_microscopy_image_as_inr(image_file, channel_names=None, saving_directory=None, saving_filename=None):
    if image_file.split('.')[-1] == 'lsm':
        img = read_lsm_image(image_file,channel_names)
    elif image_file.split('.')[-1] == 'czi':
        img = read_czi_image(image_file,channel_names)

    n_channels = len(img) if isinstance(img,dict) else 1

    filename = os.path.split(image_file)[1]
    if saving_directory is None:
        saving_directory = os.path.dirname(image_file)
    if not os.path.exists(saving_directory):
        os.makedirs(saving_directory)
    if saving_filename is None:
        saving_filename = filename

    if n_channels > 1:
        channel_names = img.keys()
        for i_channel,channel_name in enumerate(channel_names):
            inr_filename = saving_directory+"/"+os.path.splitext(saving_filename)[0]+"_" +channel_name+".inr.gz"
            imsave(inr_filename,img[channel_name])
    else:
        inr_filename = saving_directory+"/"+os.path.splitext(saving_filename)[0]+".inr.gz"
        imsave(inr_filename,img)

    return


def imread(image_file, channel_names=None):

    if image_file.split('.')[-1] == 'lsm':
        return read_lsm_image(image_file,channel_names)
    elif image_file.split('.')[-1] == 'czi':
        return read_czi_image(image_file,channel_names)
    elif image_file.split('.')[-1] in ['tif','tiff']:
        return read_tiff_image(image_file,channel_names)
    else:
        return imread(image_file)





