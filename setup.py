#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "A Python library for the detection and quantitative data extraction from microscopy images of cell nuclei"
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='tissue_nukem_3d',
    version="0.10.1",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Guillaume Cerutti",
    author_email="guillaume.cerutti@inria.fr",
    url='https://gitlab.inria.fr/mosaic/tissue_nukem_3d',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        "coverage",
        "mock",
        "nose",
        ],
    entry_points={},
    keywords='',
    
    test_suite='nose.collector',
    )
# #}
# change setup_kwds below before the next pkglts tag

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
