# -*- coding: utf-8 -*-
# -*- python -*-
#
#       Nuclei Quantification
#
#       Copyright 2015-2018 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       Mosaic Website : http://gitlab.inria.fr/mosaic
#
###############################################################################

import unittest

import numpy as np
from scipy.cluster.vq import vq

from tissue_nukem_3d.example_image import example_nuclei_image, nuclei_image_from_point_positions
from tissue_nukem_3d.nuclei_image_topomesh import nuclei_image_topomesh
from tissue_nukem_3d.growth_estimation import image_sequence_rigid_vectorfield_registration, surfacic_growth_estimation

from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe

class TestGrowthEstimation(unittest.TestCase):

    def setUp(self):
        size = 20.
        self.growth_factor = 1.1 
        img, points = example_nuclei_image(n_points=13,size=20.,nuclei_radius=1.5,nuclei_intensity=60000,return_points=True)
        
        center = (size/2.)*np.ones(3)
        rotation_theta = np.pi/10.
        rotation_matrix = np.array([[np.cos(rotation_theta),np.sin(rotation_theta),0],[-np.sin(rotation_theta),np.cos(rotation_theta),0],[0,0,1]])

        self.images = {}
        self.images['time_0'] = img
        next_points = dict(list(zip(points.keys(),[center + self.growth_factor*np.dot(rotation_matrix,(p-center)) for p in points.values()])))
        self.images['time_1'] = nuclei_image_from_point_positions(next_points,size=20.,nuclei_radius=1.5,nuclei_intensity=60000)

        self.sequence_data = {}
        for time in self.images.keys():
            topomesh, surface_topomesh = nuclei_image_topomesh({'reference':self.images[time]},reference_name='reference',signal_names=[],compute_ratios=[], radius_range=(0.8,1.4), threshold=3000, surface_mode='image', return_surface=True, microscope_orientation=1)
            self.sequence_data[time] = topomesh_to_dataframe(topomesh,0)
            self.sequence_data[time] = self.sequence_data[time][self.sequence_data[time]['layer']==1]


    def tearDown(self):
        pass

    def test_growth_estimation(self):

        transformed_images, rigid_transformations, vectorfield_transformations = image_sequence_rigid_vectorfield_registration(self.images)

        growth_sequence_data = surfacic_growth_estimation(self.sequence_data,rigid_transformations,vectorfield_transformations,maximal_length=15.,microscope_orientation=1)

        for i_time, time in enumerate(self.images.keys()):
            if i_time == 0:
                growth_variable = 'next_relative_surfacic_growth'
            else:
                growth_variable = 'previous_relative_surfacic_growth'

            assert(np.all(np.isclose(np.abs(growth_sequence_data[time][growth_variable]-np.power(self.growth_factor,2)),0,atol=0.05)))



