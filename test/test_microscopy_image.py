# -*- coding: utf-8 -*-
# -*- python -*-
#
#       Nuclei Quantification
#
#       Copyright 2015 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       TissueLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest
import os

import numpy as np

import tissue_nukem_3d
from tissue_nukem_3d.microscopy_images.read_microscopy_image import read_czi_image


class TestMicroscopyImage(unittest.TestCase):
    """
    Tests the microscopy image file format reading functions
    """

    def setUp(self):
        self.filename = os.path.abspath(str(tissue_nukem_3d.__path__[0])+"/../../share/data/images/image.czi")

    def tearDown(self):
        pass

    def test_czi_reader(self):
        img_dict = read_czi_image(self.filename)
        assert len(img_dict) == 5







