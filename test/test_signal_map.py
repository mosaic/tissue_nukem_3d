# -*- coding: utf-8 -*-
# -*- python -*-
#
#       Nuclei Quantification
#
#       Copyright 2015 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE.txt or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
#       TissueLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest
import os

import numpy as np

from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh
from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe

from tissue_nukem_3d.signal_map import SignalMap, save_signal_map, read_signal_map


class TestSignalMap(unittest.TestCase):
    """
    Tests the signal map class.
    """

    def setUp(self):
        topomesh = hexagonal_grid_topomesh(size=1,voxelsize=10.)

        self.data = topomesh_to_dataframe(topomesh, 0)
        self.data['layer'] = 1
        self.data['signal'] = 0

        self.saved_filename = "tmp/signal.map"
        if not os.path.exists("tmp/"):
            os.makedirs("tmp/")

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)


    def test_signal_map(self):
        signal_map = SignalMap(self.data, extent=30., resolution=2, radius=7.5, density_k=0.55)
        assert(np.all(signal_map.signal_map('signal')[signal_map.confidence_map>0.5]==0))

    def test_signal_map_saving(self):
        signal_map = SignalMap(self.data, extent=30., resolution=2, radius=7.5, density_k=0.55)
        save_signal_map(signal_map,self.saved_filename)
        assert os.path.exists(self.saved_filename)

        read_map = read_signal_map(self.saved_filename)

        assert np.all(read_map.xx == signal_map.xx)
        assert np.all(read_map.signal_names() == signal_map.signal_names())







