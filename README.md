# Tissue NUKEM 3D

##Authors:
* Guillaume Cerutti (<guillaume.cerutti@inria.fr>)
* Sophie Ribes (<sophie.ribes@inria.fr>)


##Institutes:
* Inria (<http://www.inria.fr>)
* RDP, ENS de Lyon - UMR 5667


##License:
* `Cecill-C`
* Uses CZI file reader developed by Christoph Gohlke <http://www.lfd.uci.edu/~gohlke/>
* Uses LSM file reader developed by Charles Roduit <charles.roduit@gmail.com>

##Description

A Python library for the detection and quantitative data extraction from microscopy images of cell nuclei.

##Requirements

* NumPy 
* SciPy
* cellcomplex (<https://gitlab.inria.fr/mosaic/cellcomplex>)

##Installation

```
python setup.py develop --user
```